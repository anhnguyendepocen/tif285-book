FROM registry.gitlab.com/islandoftex/images/texlive:latest
RUN \
  apt-get update && \
  apt-get install -qy python3-pip && \
  pip install -U \
      jupyter-book \
      jupytext \
      sphinx-inline-tabs && \
  pip install -U \
      emcee \
      gpy \
      gpyopt \
      matplotlib \
      numpy \
      scipy
